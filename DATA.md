APP
  HEADER
  MAIN

    SECTION       # overview
      Article     # account_status
      Article     # groups
      Article     # tags
      Article     # latest_reports
      Article     # activity_log

    SECTION       # all_devices
      Aside
        VIEW    # devices_actions

      Content
        VIEW    # devices_table

    SECTION       # new_reports
      Aside
        VIEW    # new_reports_list
      Content
        VIEW    # new_report_detail

    SECTION       # device
      Aside
        VIEW    # devices_list
        VIEW    # device_info
        VIEW    # reports_list
      Content     # reports_detail
        SECTION   # device_map
        SECTION   # report_detail
        SECTION   # activity_log
        SECTION   # browser_history
        SECTION   # contacts_backup

    SECTION       # global_map

    SECTION       # settings
      Aside
        Header
        Article
      Content
        SECTION   # account
        SECTION   # users
        SECTION   # security
        SECTION   # notifications
        SECTION   # summary









VIEWS
  LAYOUT
    _layout
    _navigation

  SHARED
    _map
    _actions
    _activity
    _backups

  SESIONS
    _login
    _reset

  OVERVIEW
    WIDGETS
      _account_status
      _groups
      _tags
      _latest_reports
      _activity_log
    _index

  DEVICES
    _item_table
    _item_list
    _devices_table
    _devices_list
    _device_info
    devices
    device

  REPORTS
    _list
    _table
    _detail

  SETTINGS
    _account
    _users
    _security
    _notifications
    _summary


https://github.com/Team-Sass/toolkit
https://github.com/carloselias/perkins/blob/master/assets/_core.less

https://github.com/twbs/bootstrap-sass/blob/master/vendor/assets/stylesheets/bootstrap/_variables.scss

http://purecss.io/menus/
http://semantic-ui.com/?ref=plentyofframeworks

https://github.com/cferdinandi/kraken/blob/master/sass/components/_buttons.scss

http://getuikit.com/index.html
http://gumbyframework.com/docs/mixins/#!/introduction
http://foundation.zurb.com/




//$tone-base:              #434A54;
//$tone-darkest:           darken($tone-base, 99%);
//$tone-darker:            darken($tone-base, 66%);
//$tone-dark:              darken($tone-base, 33%);
//$tone:                   $tone-base;
//$tone-light:             lighten($tone-base, 33%);
//$tone-lighter:           lighten($tone-base, 66%);
//$tone-lightest:          lighten($tone-base, 99%);
//
//$tone-darkest:           scale-color($tone-base, $lightness:8%);
//$tone-darker:            scale-color($tone-base, $lightness:22%);
//$tone-dark:              scale-color($tone-base, $lightness:36%);
//$tone:                   scale-color($tone-base, $lightness:50%);
//$tone-light:             scale-color($tone-base, $lightness:64%);
//$tone-lighter:           scale-color($tone-base, $lightness:78%);
//$tone-lightest:          scale-color($tone-base, $lightness:92%);


//@p-sans:                          sans-serif;
//@p-serif:                         serif;
//@p-mono:                          mono-space, monospace;
//@p-headings:                      @p-sans;
//
//@p-basefont:                      1.6rem;
//@p-baseline:                      1.4;
//
//@p-page-width:                    100%;
//@p-grid-columns:                  16;
//@p-grid-margin:                   1%;
//
//@p-white:                         #fff;
//@p-black:                         #222;
//
//@p-palette-desaturation:          0;
//@p-palette-lightness:             0;
//@p-palette-hue:                   0;
