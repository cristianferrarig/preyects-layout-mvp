# Default encoding
set :default_encoding, 'utf-8'
Encoding.default_external = 'utf-8' if Object.const_defined?(:Encoding)

# For physical directories at development mode
set :markdown, :layout_engine => :haml

activate :livereload
activate :autoprefixer
activate :directory_indexes

#activate :autoprefixer, browsers: ['last 2 versions', 'ie 8', 'ie 9']

###
# Compass
###

# Susy grids in Compass
# First: gem install susy
# require 'susy'

# Change Compass configuration
# compass_config do |config|
#   config.output_style = :compact
# end

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
# page "/path/to/file.html", :layout => false
#
# With alternative layout
# page "/path/to/file.html", :layout => :otherlayout
#
# A path which all have the same layout
# with_layout :admin do
#   page "/admin/*"
# end

# Proxy (fake) files
# page "/this-page-has-no-template.html", :proxy => "/template-file.html" do
#   @which_fake_page = "Rendering a fake page with a variable"
# end

###
# Helpers
###

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes

# Methods defined in the helpers block are available in templates
helpers do


  def select_all_button(target)
    content_tag(:label, class: "checkbox btn btn-default pull-left") do
      content_tag(:input, '', type: "checkbox", class: "js-select-all", data: {selection: "#"+target})+
      content_tag(:span, "")
    end
  end

  def dropdown_title(title)
    title + " " + content_tag(:span, "", class: "caret")
  end

  def dropdown_button(title,&block)
    content_tag(:button, class: 'btn btn-default dropdown-toggle', type: 'button', data: {toggle: "dropdown"}) do
      title + content_tag(:span, '', class: 'caret')
    end +
    capture_html(&block).html_safe
  end

  def empty_view(icon,text)
    content_tag(:div, class: 'empty-view') do
      content_tag(:div, class: 'empty-content') do
        content_tag(:i, '', class: 'icon-'+icon)+
        content_tag(:h3, text)
      end
    end
  end


  def master(tag,id,opts={},&block)
    content_tag(tag, class: 'btn btn-default dropdown-toggle', type: 'button', data: {toggle: "dropdown"})
  end


  def section(section_id,&block)
    content_tag(:section, id: section_id) do
      if block_given?
        capture(&block)
      else
        partial_path = "sections/"+section_id+".haml"
        partial partial_path
      end
    end
  end

end


# For physical directories at development mode
set :images_dir,  "assets/images"
set :fonts_dir,  "assets/fonts"
set :css_dir,  "assets/stylesheets"
set :js_dir, "assets/javascripts"

# Build-specific configuration
configure :build do
  # ignore 'images/*.psd'
  # ignore 'stylesheets/lib/*'
  # ignore 'stylesheets/vendor/*'
  # ignore 'javascripts/lib/*'
  # ignore 'javascripts/vendor/*'

  # For example, change the Compass output style for deployment
  activate :minify_css

  # Minify Javascript on build
  activate :minify_javascript

  # Enable cache buster
  # activate :cache_buster

  # Use relative URLs
  # activate :relative_assets

  # Compress PNGs after build
  # First: gem install middleman-smusher
  # require "middleman-smusher"
  # activate :smusher

  # Or use a different image path
  # set :http_path, "/Content/images/"
end
